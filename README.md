# Spike-Ai-GUI
This is the spike for create this [project](https://gitlab.com/nicolalandro/ai_block/tree/master).

[Users Docs](https://gitlab.com/nicolalandro/spike-ai-gui/wikis/Spike-AI-Block).

## Come utilizzare
    $ pip3 install -r requirements.txt
    $ honcho start

## Backend
* flask
* honcho

## Frontend
* vue
* bootstrap-vue
* cytoscape 3.2.20 ([DOC](http://js.cytoscape.org/#cy.on))
* ace ([code](https://github.com/ajaxorg/ace-builds/tree/c915483e909bc1b3061cabaaf932f796c723226b/src))

## BUG
Attualmente vuoto

## Miglioramenti SW
* FRONT-END
  * usare webpack per import js
  * drag & drop per aggiungere nodi
  * usare pug invece di html  (NTH)
* BACK-END
  * usare gunicorn
  * migliorare gli errori python con exec

## Constraint
Js Code
* la variabile "vue.element_data.properties", può essere utilizzata per salvare le proprietà di un nodo.
  * tutte le variabili salvate al suo interno vengono visualizzate con lo stesso nome nel codice python
  * queste variabili vengono automaticamente ricaricate anche nella vista se sono presenti degli elementi che hanno l'id uguale al nome della variabile
  * per inibire questo caricamento automatico "vue.element_data.properties.not_reload_state = true;"
  
Python Code:
* la variabile "out" contiene tutti gli output a console di un nodo
* (tutte le variabili definite dal javascript in vue.element_data.properties)

## Release container
    $ release_container.sh
Il container rilasciato ha come tag il commit corrente della shell (git rev-parse HEAD)