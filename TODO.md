# Cross validation
* deve funzionare N volte, quindi ciclare
  * capire se le N volte sono generali o sul batch
  * cross validation fold
* il parametro 'test_size' deve essere settato dalla gui

# Neural Network
* 'loss' function selezionabile dalla gui
* 'optimizer' selezionabile dalla gui
* 'metrics' selezionabile dalla gui?

# Train object
* batch size
* epoche

# Leggere file DICOM
* formato standard immagini mediche

# Metrice di accuratezza imagini
* jacquard
* sensitivity
* specifity
* recall
* accuracy
* matrice di confusione
* matrice di correlazione
