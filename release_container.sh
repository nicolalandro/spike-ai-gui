#!/usr/bin/env bash
docker build -t registry.gitlab.com/nicolalandro/spike-ai-gui:$(git rev-parse HEAD) .
docker push registry.gitlab.com/nicolalandro/spike-ai-gui:$(git rev-parse HEAD)
