import json
import os

from flask import Flask, render_template, request, jsonify

from src.graph import Graph

app = Flask(__name__)


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/exec', methods=['POST'])
def exec_graph():
    req = request.get_json()
    try:
        g = Graph(req)
        output = g.play()
        return str(output)
    except Exception as e:
        return "ERROR: " + str(e)


@app.route('/get_saved_node_by_name', methods=['POST'])
def get_saved_node_by_name():
    name = request.get_json()['name']
    try:
        with open("static/nodes/%s.json" % name, 'r') as infile:
            js = infile.read()
        return jsonify({'status': 'ok', 'node': js, 'message': 'SUCCESS'})
    except Exception as e:
        return jsonify({'status': 'error', 'message': str(e)})


@app.route('/saved_nodes')
def saved_nodes():
    ls_dir = os.listdir('static/nodes')
    name_without_type = list(map(lambda n: n[:-5], ls_dir))
    return jsonify(name_without_type)


@app.route('/save_node', methods=['POST'])
def save_node():
    req = request.get_json()
    with open("static/nodes/%s.json" % req['data']['name'], 'w') as outfile:
        json.dump(req, outfile)
    return 'SUCCESS'


@app.route('/saved_files')
def saved_files():
    ls_dir = os.listdir('static/storage')
    name_without_type = list(map(lambda n: n[:-5], ls_dir))
    return jsonify(name_without_type)


@app.route('/save', methods=['POST'])
def save_graph():
    req = request.get_json()
    with open("static/storage/%s.json" % req['name'], 'w') as outfile:
        json.dump(req['graph'], outfile)
    return 'SUCCESS'


@app.route('/load', methods=['POST'])
def load_graph():
    req = request.get_json()
    try:
        with open("static/storage/%s.json" % req['name'], 'r') as infile:
            js = infile.read()
        return jsonify({'status': 'ok', 'graph': js, 'message': 'SUCCESS'})
    except Exception as e:
        return jsonify({'status': 'error', 'message': str(e)})


if __name__ == "__main__":
    app.config['TEMPLATES_AUTO_RELOAD'] = True
    app.jinja_env.auto_reload = True
    app.run('localhost', 5000, use_reloader=True, use_debugger=True)
