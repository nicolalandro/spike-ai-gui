let DEFAULT_INFO_TEXT = 'No element selected'

window.onload = () => {
  var vue = window.vue = new Vue({
    el: '#app',
    delimiters: ['#{', '}'],
    mounted() {
      http_request('GET','/saved_files', {}, this.populate_all_graphs_names)
      http_request('GET','/saved_nodes', {}, this.populate_saved_nodes)
      this.cy = cytoscape({
        container: document.getElementById('cy'),
        elements: [],
        layout: {
          name: 'circle'
        },
        style: create_style()
      });
      this.cy.on('tap', 'node', function (evt) {
        ev = selected = this
        if (selected.length > 0) {
          vue.element_selected = true
          vue.edge_selected = false
          vue.cy_element = selected[0]
          vue.element_data = selected[0].json()['data']
          if(!selected[0].json()['data'].properties) {
            vue.element_data.properties = {};
          }
          vue.reload_html_state()
        }
      });
      this.cy.on('tap', 'edge', function (evt) {
        ev = selected = this
        if (selected.length > 0) {
            vue.element_selected = false
            vue.edge_selected = true
            vue.cy_edge= selected[0]
            vue.edge_data = selected[0].json()['data']
        }
      });
      this.cy.on('tap', function (event) {
        var evtTarget = event.target;
        if (evtTarget === vue.cy) {
          vue.element_selected = false
          vue.edge_selected = false
        }
      });
    },
    data: {
      normal_shapes: ['ellipse','triangle','rectangle','roundrectangle','bottomroundrectangle','cutrectangle',
                      'barrel','rhomboid','diamond','pentagon','hexagon','concavehexagon','heptagon',
                      'octagon','star', 'tag', 'vee'],
      saved_nodes: [],
      all_graphs_names: [],
      graph_name: '',
      console_message: '',
      cy: null,
      element_selected: false,
      cy_element: null,
      element_data: {
        id: null,
        name: null,
        weight: null,
        faveColor: null,
        faveShape: null,
        pyCode: '',
        htmlCode: '',
        properties: {}
      },
      edge_selected: false,
      cy_edge: null,
      edge_data: {
        id: null,
        faveColor: null,
        mapVariable: ''
      }
    },
    methods: {
      not_implemented(){
        alert('Not Implemented')
      },
      play(){
        http_request('POST','/exec', this.cy.json(), this.clean_and_put_message_in_console)
      },
      save(){
        http_request('POST','/save', {'name': this.graph_name,'graph':this.cy.json()}, this.clean_and_put_message_in_console)
      },
      load(){
        http_request('POST','/load', {'name': this.graph_name}, this.load_cy)
      },
      populate_all_graphs_names(data){
         this.all_graphs_names = JSON.parse(data)
      },
      save_node(){
        http_request('POST','/save_node', this.cy_element.json(), this.clean_and_put_message_in_console)
      },
      populate_saved_nodes(data){
        this.saved_nodes = JSON.parse(data)
      },
      prevent_tab_py(event){
        if (event) {
            event.preventDefault()
            let startText = this.element_data.pyCode.slice(0, event.target.selectionStart)
            let endText = this.element_data.pyCode.slice(event.target.selectionStart)
            this.element_data.pyCode = `${startText}\t${endText}`
            event.target.selectionEnd = event.target.selectionStart + 1
        }
      },
      prevent_tab_html(event){
        if (event) {
            event.preventDefault()
            let startText = this.element_data.htmlCode.slice(0, event.target.selectionStart)
            let endText = this.element_data.htmlCode.slice(event.target.selectionStart)
            this.element_data.htmlCode = `${startText}\t${endText}`
            event.target.selectionEnd = event.target.selectionStart + 1
        }
      },
      prevent_tab_js(event){
        if (event) {
            event.preventDefault()
            let startText = this.element_data.jsCode.slice(0, event.target.selectionStart)
            let endText = this.element_data.jsCode.slice(event.target.selectionStart)
            this.element_data.jsCode = `${startText}\t${endText}`
            event.target.selectionEnd = event.target.selectionStart + 1
        }
      },
      reload_html_state(){
        sleep(100).then(() => {
            if(! this.element_data.properties.not_reload_state){
                for (var key in this.element_data.properties) {
                    document.querySelector("#html #" + key).value = this.element_data.properties[key]
                }
            }
            eval(this.element_data.jsCode)
        });
      },
      get_saved_node_cy(event){
        node_names = event.currentTarget.id;
        http_request('POST','/get_saved_node_by_name', {'name': node_names}, this.add_saved_node_cy)
      },
      add_saved_node_cy(data){
        data = JSON.parse(data)
        if(data['status'] == 'ok'){
            node = JSON.parse(data['node'])
            delete node['data']['id']
            this.cy.add(node);
            this.console_message=data['message'];
        }
        else{
            this.console_message=data['message'];
        }
      },
      load_cy(data){
        data = JSON.parse(data)
        if(data['status'] == 'ok'){
            this.cy.elements().remove();
            this.cy.json(JSON.parse(data['graph']));
            this.console_message=data['message'];
        }
        else{
            this.console_message=data['message'];
        }
      },
      fit_cy() {
        this.cy.fit();
      },
      add_node_cy() {
        this.cy.add([{
          group: 'nodes',
          data: { name: random_string(), weight: 70, faveColor: '#6FB1FC', faveShape: 'rectangle', pyCode: '' , htmlCode: ''},
          position: { x: 100, y: 100 }
        }]);
      },
      add_edge_left_cy() {
        selected = this.cy.$(':selected')
        if (selected.length == 2) {
          this.cy.add([{
            group: 'edges',
            data: {
              mapVariable: '',
              faveColor: '#F5A45D', strength: 90,
              source: selected[0].id(), target: selected[1].id()
            }
          }]);
        }
        else {
          this.console_message='select two nodes';
        }
      },
      add_edge_right_cy() {
        selected = this.cy.$(':selected')
        if (selected.length == 2) {
          this.cy.add([{
            group: 'edges',
            data: {
              mapVariable: '',
              faveColor: '#F5A45D', strength: 90,
              source: selected[1].id(), target: selected[0].id()
            }
          }]);
        }
        else {
          this.console_message='select two nodes';
        }
      },
      remove_cy() {
        this.cy.$(':selected').remove()
        this.element_selected = false
      },
      update_element_cy(){
        if(this.element_selected){
            this.element_data.pyCode = this.element_data.pyCode.replace('  ', '\t')
            this.cy_element.data(this.element_data)
        }
        else if(this.edge_selected){
            this.cy_edge.data(this.edge_data)
        }
      },
      clean_and_put_message_in_console(msg){
         this.console_message=msg;
      },
      on_python_editor_update(code, component){
        this.element_data.pyCode = code;
      },
      on_html_editor_update(code, component){
        this.element_data.htmlCode = code;
      },
      on_js_editor_update(code, component){
        this.element_data.jsCode = code;
      }
    }
  });
}

Vue.component('xeditor', {
    template: '<div :style="{height: height, width: width}"></div>',
    props: {
        value: {
          required: true
        },
        lang: {
          type: String,
          default: 'html'
        },
        theme: {
          type: String,
          default: 'dracula'
        },
        height: {
          type: String,
          default: '250px'
        },
        width: {
          type: String,
          default: '100%'
        },
        sync: {
          type: Boolean,
          default: false
        }
    },

    data: function () {
        return {
          editor: null
        };
     },
      mounted: function () {
        this.editor = ace.edit(this.$el);
        this.editor.$blockScrolling = Infinity;
        this.editor.getSession().setMode('ace/mode/' + this.lang);
        this.editor.setTheme('ace/theme/' + this.theme);
        if(this.value){
            this.editor.setValue(this.value, 1);
        }
        this.editor.on('change', () => {
            try {
                this.$emit('x-editor-update', this.editor.getValue(), this);
            }catch(e) {
                console.log(e);
            }
        });
      },
      watch: {
        value: function (newValue) {
          window.editor = this.editor;
          let curr = this.editor.getCursorPosition();
          let row = curr.row;
          let column = curr.column;
          if (this.sync) {
            this.editor.setValue(newValue, 1);
            this.editor.moveCursorTo(row, column);
          }
          else {
            if(newValue){
                this.editor.setValue(newValue, 1);
                this.editor.moveCursorTo(row, column);
            }else {
                this.editor.setValue('', 1);
            }
          }
        }
      }
});

function create_style() {
  return cytoscape.stylesheet()
    .selector('node')
    .css({
      'shape': 'data(faveShape)',
      'width': 'mapData(weight, 40, 80, 20, 60)',
      'content': 'data(name)',
      'text-valign': 'center',
      'text-outline-width': 2,
      'text-outline-color': 'data(faveColor)',
      'background-color': 'data(faveColor)',
      'color': '#fff'
    })
    .selector(':selected')
    .css({
      'border-width': 3,
      'border-color': '#333'
    })
    .selector('edge')
    .css({
      'curve-style': 'bezier',
      'opacity': 0.666,
      'width': 'mapData(strength, 70, 100, 2, 6)',
      'target-arrow-shape': 'triangle',
      'source-arrow-shape': 'circle',
      'line-color': 'data(faveColor)',
      'source-arrow-color': 'data(faveColor)',
      'target-arrow-color': 'data(faveColor)'
    })
    .selector('edge:selected')
    .css({
      'border-width': 3,
      'line-color': '#333',
      'source-arrow-color': '#333',
      'target-arrow-color': '#333'
    })
    .selector('edge.questionable')
    .css({
      'line-style': 'dotted',
      'target-arrow-shape': 'diamond'
    })
    .selector('.faded')
    .css({
      'opacity': 0.25,
      'text-opacity': 0
    })
}

function random_string() {
  var text = '';
  var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

  for (var i = 0; i < 6; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;
}

function http_request(method, url, body, success_function){
    let xhr = new XMLHttpRequest();

    xhr.onreadystatechange = function () {
        if (this.readyState != 4) return;

        if (this.status == 200) {
            let data = this.responseText;
            success_function(data);
        }
    };

    xhr.open(method, url, true);
    xhr.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
    xhr.send(JSON.stringify(body));
}

Vue.component('html-custom', {
    props: ['legacySystemHtml'],
    template: '<div v-html="legacySystemHtml"></div>'
})

function sleep(milliseconds) {
  return new Promise(resolve => setTimeout(resolve, milliseconds))
}
