import json


class Graph:
    def __init__(self, graph):
        self.graph = graph

    def play(self):
        output_string = ''
        variable_hash = {}
        ex_plan = self.execution_plan()
        for id, dependency in ex_plan:
            input = self.create_input(id, dependency, variable_hash)
            output = {}
            pycode = self.get_pycode_by_id(id)
            exec(pycode, input, output)
            variable_hash[id] = output
            if 'out' in output:
                output_string += str(output['out']) + '\n'
        return output_string

    def get_pycode_by_id(self, id):
        pycode = ''
        for d in self.graph['elements']['nodes']:
            if d['data']['id'] == id:
                pycode = d['data']['pyCode']
        return pycode

    def execution_plan(self):
        nodes = self.graph['elements']['nodes']
        edges = self.graph['elements']['edges']
        processable_nodes_id = self.processable_nodes(edges, nodes)
        node_exec_plan = []
        processable_edges = []
        while len(processable_nodes_id) != 0:
            processable_id = processable_nodes_id[0]

            processable_edges += list(filter(lambda e: e['data']['source'] == processable_id, edges))
            selected_edges = list(filter(lambda e: e['data']['target'] == processable_id, processable_edges))
            selected_data = list(map(lambda e: (e['data']['source'], e['data']['mapVariable']), selected_edges))
            node_exec_plan.append((processable_id, selected_data))

            nodes = list(filter(lambda n: n['data']['id'] != processable_id, nodes))
            edges = list(filter(lambda e: e['data']['source'] != processable_id, edges))
            processable_nodes_id = self.processable_nodes(edges, nodes)
        return node_exec_plan

    def processable_nodes(self, edges, nodes):
        nodes_ids = set(map(lambda n: n['data']['id'], nodes))
        target_nodes = set(map(lambda e: e['data']['target'], edges))
        return list(nodes_ids - target_nodes)

    def create_input(self, id, dependency, variable_hash):
        input = {}
        for dependency_id, map_vars in dependency:
            for map_var in list(filter(None, map_vars.split(';'))):
                value = map_var.split(':')
                input[value[1]] = variable_hash[dependency_id][value[0]]
        node = list(filter(lambda n: n['data']['id'] == id, self.graph['elements']['nodes']))[0]['data']
        if 'properties' in node:
            for k in node['properties']:
                input[k] = node['properties'][k]
        return input


if __name__ == '__main__':
    with open('../static/storage/saved.json', 'r') as infile:
        json_string = infile.read()
    js = json.loads(json_string)
    g = Graph(js)
    # text = g.execution_plan()
    text = g.play()
    print(text)
