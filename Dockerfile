FROM tensorflow/tensorflow:1.12.0-py3

WORKDIR /app

RUN pip install --upgrade pip
COPY requirements.txt /app/
RUN pip install --no-cache-dir -r requirements.txt

COPY src /app/src
COPY static /app/static
COPY templates /app/templates
COPY main.py /app/
COPY Procfile /app/

CMD honcho start
